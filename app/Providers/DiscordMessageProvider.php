<?php

namespace App\Providers;

use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class DiscordMessageProvider extends ServiceProvider
{
    public static function sendMessage(string $message, string $hook)
    {
        $client = new Client();
        $client->post(
            $hook,
            [
                'verify' => false,
                'json' => ['content' => $message]
            ]
        );
    }
}
