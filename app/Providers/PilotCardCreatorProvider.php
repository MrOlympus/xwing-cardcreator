<?php

namespace App\Providers;

define('CARD_PILOT_W', 298);
define('CARD_PILOT_H', 417);
define('CARD_ART_PILOT_W', 298);
define('CARD_ART_PILOT_H', 127);

use Exception;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class PilotCardCreatorProvider extends ServiceProvider implements CardCreator
{

    public function register()
    {

    }

    public static function processFiles(array $files): array
    {
        $processedFiles = [];

        if (isset($files['card-art-image'])) {
            if ($files['card-art-image']['type'] == 'image/jpeg') {
                $processedFiles['imageCardArt'] = imagecreatefromjpeg($files['card-art-image']['tmp_name']);
            } elseif ($files['card-art-image']['type'] == 'image/png') {
                $processedFiles['imageCardArt'] = imagecreatefrompng($files['card-art-image']['tmp_name']);
            }
        } else {
            $processedFiles['imageCardArt'] = imagecreatetruecolor(1, 1);
        }

        return $processedFiles;
    }

    /**
     * @param string[] $params
     * @return string[]
     */
    public static function processParams(array $params): array
    {
        $params['card-name'] = strtoupper((isset($params['card-name']) && $params['card-name'] != '' ? $params['card-name'] : ''));
        $params['pilot-title'] = isset($params['pilot-title']) && $params['pilot-title'] != '' ? $params['pilot-title'] : '';

        $params['pilot-ability'] = isset($params['pilot-ability']) && $params['pilot-ability'] != '' ? $params['pilot-ability'] : '';
        $params['ship-ability'] = isset($params['ship-ability']) && $params['ship-ability'] != '' ? $params['ship-ability'] : '';

        $params['faction'] = isset($params['faction']) && $params['faction'] != '' ? $params['faction'] : 'empire';
        $params['ship'] = isset($params['ship']) && $params['ship'] != '' ? $params['ship'] : 'tiefighter';

        $params['attack-front'] = isset($params['attack-front']) && $params['attack-front'] != '' ? $params['attack-front'] : 0;
        $params['attack-front'] = self::checkMinMax($params['attack-front'], 0, 5);
        $params['attack-rear'] = isset($params['attack-rear']) && $params['attack-rear'] != '' ? $params['attack-rear'] : 0;
        $params['attack-rear'] = self::checkMinMax($params['attack-rear'], 0, 5);
        $params['attack-singleMobile'] = isset($params['attack-singleMobile']) && $params['attack-singleMobile'] != '' ? $params['attack-singleMobile'] : 0;
        $params['attack-singleMobile'] = self::checkMinMax($params['attack-singleMobile'], 0, 5);
        $params['attack-doubleMobile'] = isset($params['attack-doubleMobile']) && $params['attack-doubleMobile'] != '' ? $params['attack-doubleMobile'] : 0;
        $params['attack-doubleMobile'] = self::checkMinMax($params['attack-doubleMobile'], 0, 5);

        $params['agility'] = isset($params['agility']) && $params['agility'] != '' ? $params['agility'] : 0;
        $params['agility'] = self::checkMinMax($params['agility'], 0, 5);
        $params['hull'] = isset($params['hull']) && $params['hull'] != '' ? $params['hull'] : 0;
        $params['hull'] = self::checkMinMax($params['hull'], 1, 15);
        $params['shield'] = isset($params['shield']) && $params['shield'] != '' ? $params['shield'] : 0;
        $params['shield'] = self::checkMinMax($params['shield'], 0, 10);
        $params['force'] = isset($params['force']) && $params['force'] != '' ? $params['force'] : 0;
        $params['force'] = self::checkMinMax($params['force'], 0, 6);
        $params['charge'] = isset($params['charge']) && $params['charge'] != '' ? $params['charge'] : 0;
        $params['charge'] = self::checkMinMax($params['charge'], 0, 6);

        $params['actions'] = isset($params['actions']) && $params['actions'] != '' ? $params['actions'] : 'foc,none,none,none,none';
        $params['actions-red'] = isset($params['actions-red']) && $params['actions-red'] != '' ? $params['actions-red'] : 'none,none,none,none,none';
        $params['actions'] = explode(',', $params['actions']);
        $params['actions-red'] = explode(',', $params['actions-red']);

        $params['initiative'] = isset($params['initiative']) && $params['initiative'] != '' ? $params['initiative'] : 1;
        $params['initiative'] = self::checkMinMax($params['initiative'], 1, 6);

        return $params;
    }

    public static function createCard(array $params, array $files, string $id, bool $privatePath = false)
    {
        try {
            // ----------------------------------- Font loading
            $fonts = [
                'main' => 'arial.ttf',
                'title' => 'Orbitron-Bold.ttf',
                'bold' => 'arialbd.ttf',
                'italic' => 'ariali.ttf',
                'boldItalic' => 'arialbi.ttf',
                'symbol' => 'xwing-miniatures-modified.ttf',
                'ship' => 'xwing-miniatures-ships.ttf'
            ];

            foreach ($fonts as $name => $path) {
                $fonts[$name] = CardCreatorProvider::loadRessource($path);
            }

            // ----------------------------------- Image loading

            $images = [
                'abilitySeparator' => 'pilot-ability-separator-large.png',
                'agility' => 's_agility.png',
                'attack-doubleMobile' => 's_dualMobileArc.png',
                'attack-rear' => 's_rearArc.png',
                'attack-singleMobile' => 's_mobileArc.png',
                'base' => 'base-pilots-' . $params['faction'] . '.png',
                'baseLinkedAction' => 'base-linked-actions-' . $params['faction'] . '.png',
                'charge' => 's_charge.png',
                'force' => 's_force.png',
                'attack-front' => 's_frontArc.png',
                'hull' => 's_hull.png',
                'separator' => 'pilot-action-separator-' . $params['faction'] . '.png',
                'shield' => 's_shield.png'
            ];

            foreach ($images as $name => $path) {
                $images[$name] = CardCreatorProvider::loadImagePng($path);
            }

            $images['cardArt'] = $files['imageCardArt'];

            // ----------------------------------- Card base (art + background)

            $im = imagecreatetruecolor(CARD_PILOT_W, CARD_PILOT_H);
            imagefill($im, 0, 0, 0x7fff0000);

            $src_w = imagesx($images['cardArt']);
            $src_h = imagesy($images['cardArt']);

            // Resizing the art
            if (imagesx($images['cardArt']) / imagesy($images['cardArt']) < CARD_ART_UPGRADE_W / CARD_ART_UPGRADE_H) {
                $src_h = $src_h / ((CARD_ART_UPGRADE_W / CARD_ART_UPGRADE_H) / (imagesx($images['cardArt']) / imagesy($images['cardArt'])));
            } else {
                $src_w = $src_w * ((CARD_ART_UPGRADE_W / CARD_ART_UPGRADE_H) / (imagesx($images['cardArt']) / imagesy($images['cardArt'])));
            }

            imagecopyresampled($im, $images['cardArt'], 0, 0, 0, 0, 298, 127, $src_w, $src_h);
            imagecopyresampled($im, $images['base'], 0, 0, 0, 0, CARD_PILOT_W, CARD_PILOT_H, CARD_PILOT_W, CARD_PILOT_H);

            // ----------------------------------- Color creation

            $colors = [
                'background' => [0, 0, 0],
                'black' => [46, 46, 46],
                'darkGray' => [70, 70, 70],
                'orange' => [230, 118, 43],
                'white' => [255, 255, 255],
                'green_agility' => [106, 189, 69],
                'red_attack' => [238, 32, 36],
                'yellow_charge' => [253, 192, 16],
                'purple_force' => [195, 157, 201],
                'yellow_hull' => [247, 236, 20],
                'blue_shield' => [125, 208, 226]
            ];

            foreach ($colors as $name => $value) {
                $colors[$name] = imagecolorallocate($im, $value[0], $value[1], $value[2]);
            }

            // ----------------------------------- Card name and pilot title
            $processedText = CardCreatorProvider::processTextSymbols($params['card-name'], 10, $fonts, 'title');
            CardCreatorProvider::writeTextBlockCenter($im, 57, 240, 131, 158, $processedText, $colors['white']);

            $processedText = CardCreatorProvider::processTextSymbols($params['pilot-title'], 8, $fonts, 'italic');
            CardCreatorProvider::writeTextBlockCenter($im, 79, 219, 161, 178, $processedText, $colors['white']);

            // ----------------------------------- Initiative

            $processedText = CardCreatorProvider::processTextSymbols($params['initiative'], 18, $fonts, 'title');
            CardCreatorProvider::writeTextBlockCenter($im, 8, 35, 133, 167, $processedText, $colors['orange']);

            // ----------------------------------- Ship image and name

            $nameSymbol = ShipStatProvider::getShipFullNameAndSymbol($params['ship']);
            $processedText = CardCreatorProvider::processTextSymbols(strtoupper($nameSymbol['fullName']), 6, $fonts, 'title');
            CardCreatorProvider::writeTextBlockCenter($im, 55, 238, 389, 401, $processedText, $colors['white']);

            $processedText = CardCreatorProvider::processTextSymbols($nameSymbol['symbol'], 19, $fonts, 'ship');
            CardCreatorProvider::writeTextBlockCenter($im, 8, 37, 388, 413, $processedText, $colors['white']);

            // ----------------------------------- Actions

            $actionsRow = 0;
            $linkedActions = false;
            for ($i = 0; $i < 5; $i++) {
                if ($params['actions'][$i] != 'none' || $params['actions-red'][$i] != 'none') {
                    $actionsRow++;
                }
                if ($params['actions'][$i] != 'none' && $params['actions-red'][$i] != 'none') {
                    $linkedActions = true;
                }
            }

            if ($linkedActions == true) {
                imagecopyresampled($im, $images['baseLinkedAction'], 203, 177, 0, 0, 95, 203, 95, 203);
            }

            $y_top = 180;
            $y_bottom = 380;

            for ($i = 1; $i < $actionsRow; $i++) {
                $y = $y_top + $i * ($y_bottom - $y_top) / $actionsRow;
                if ($linkedActions == true) {
                    imagecopyresampled($im, $images['separator'], 220, $y, 0, 0, 78, 4, 78, 4);
                } else {
                    imagecopyresampled($im, $images['separator'], 253, $y, 0, 0, 45, 4, 45, 4);
                }
            }

            $j = 0;
            $x_left = $linkedActions ? 220 : 253;
            for ($i = 0; $i < 5; $i++) {

                $y = $y_top + $j * ($y_bottom - $y_top) / $actionsRow;

                if ($params['actions'][$i] != 'none' && $params['actions-red'][$i] == 'none') {
                    $processedText = CardCreatorProvider::processTextSymbols($params['actions'][$i], 14, $fonts);
                    CardCreatorProvider::writeTextBlockCenter($im, $x_left, 300, $y, $y + ($y_bottom - $y_top) / $actionsRow, $processedText, $colors['white']);

                    $j++;

                } else if ($params['actions'][$i] == 'none' && $params['actions-red'][$i] != 'none') {

                    $processedText = CardCreatorProvider::processTextSymbols($params['actions-red'][$i], 14, $fonts, 'symbol');
                    CardCreatorProvider::writeTextBlockCenter($im, $x_left, 300, $y, $y + ($y_bottom - $y_top) / $actionsRow, $processedText, $colors['white']);

                    $j++;

                } else if ($params['actions'][$i] != 'none' && $params['actions-red'][$i] != 'none') {

                    $x_mid = (300 + $x_left) / 2;

                    $y_bottom_row = $y + ($y_bottom - $y_top) / $actionsRow;

                    $processedText = CardCreatorProvider::processTextSymbols($params['actions'][$i], 13, $fonts, 'symbol');
                    CardCreatorProvider::writeTextBlockCenter($im, $x_left, $x_mid, $y, $y_bottom_row, $processedText, $colors['white']);

                    $processedText = CardCreatorProvider::processTextSymbols('!lnk', 11, $fonts);
                    CardCreatorProvider::writeTextBlockCenter($im, $x_left, 300, $y, $y_bottom_row, $processedText, $colors['white']);

                    $processedText = CardCreatorProvider::processTextSymbols($params['actions-red'][$i], 13, $fonts);
                    CardCreatorProvider::writeTextBlockCenter($im, $x_mid, 300, $y, $y_bottom_row, $processedText, $colors['red_attack']);

                    $j++;
                }
            }

            // ----------------------------------- Abilities

            $x_left = $linkedActions ? 200 : 237;
            $y_separation = 252;

            $processedText = CardCreatorProvider::processTextSymbols($params['pilot-ability'], 8, $fonts);
            CardCreatorProvider::writeTextBlockCenter($im, 15, $x_left, 184, $y_separation, $processedText, $colors['black']);

            $processedText = CardCreatorProvider::processTextSymbols($params['ship-ability'], 9, $fonts);
            CardCreatorProvider::writeTextBlockCenter($im, 15, $x_left, $y_separation, 318, $processedText, $colors['black']);

            // ----------------------------------- Stats

            $numberOfStats = 0;

            $stats = [];

            foreach ($params as $key => $value) {
                if (in_array($key, ['attack-front', 'attack-rear', 'attack-singleMobile',
                        'attack-doubleMobile', 'hull', 'shield', 'force', 'charge']) && $value > 0) {
                    $stats[$key] = $value;
                    $numberOfStats++;
                } else if ($key === 'agility') {
                    $stats[$key] = $value;
                    $numberOfStats++;
                }
            }


//            dd([$params, $stats]);

            $x_left = 0;
            $x_right = $linkedActions ? 217 : 250;
            // Reduction
            $x_left += (9 - $numberOfStats) * 0.04 * $x_right;
            $x_right -= (9 - $numberOfStats) * 0.04 * $x_right;

            $width = ($x_right - $x_left) / $numberOfStats;

            $i = 0;
            foreach ($stats as $key => $value) {
                if ($value != 0 || $key == 'agility') {
                    $offset = $i * $width;
                    if (strpos($key, 'attack') !== false) {
                        $color = $colors['red_attack'];
                    } else {
                        switch ($key) {
                            case 'hull':
                                $color = $colors['yellow_hull'];
                                break;
                            case 'agility':
                                $color = $colors['green_agility'];
                                break;
                            case 'shield':
                                $color = $colors['blue_shield'];
                                break;
                            case 'force':
                                $color = $colors['purple_force'];
                                break;
                            case 'charge':
                                $color = $colors['yellow_charge'];
                                break;
                            default:
                                $color = $colors['white'];
                        }
                    }

                    CardCreatorProvider::drawImageCenter($im, $x_left + $offset, $x_left + $offset + (1 * $width), 325, 380, 32, 32, $images[$key]);

                    $processedText = CardCreatorProvider::processTextSymbols($stats[$key], 16, $fonts, 'title');
                    CardCreatorProvider::writeTextBlockCenter($im, $x_left + $offset, $x_left + $offset + (1 * $width), 338, 393, $processedText, $color);

                    $i++;
                }
            }

            // ----------------------------------- Output

            ob_start();
            imagepng($im);
            imagealphablending($im, false);
            imagesavealpha($im, true);
            $contents = ob_get_contents();
            ob_end_clean();

            $path = CardCreatorProvider::saveImageOnDiskTemp($contents, $id, $privatePath);
            CardCreatorProvider::cleanTempImages();

            return [
                'success' => true,
                'data' => ['path' => $path]
            ];
        } catch (Exception $e) {
            return [
                'success' => false,
                'exception' => $e->getFile() . ' ' . $e->getLine() . ' : ' . $e->getMessage(),
                'full' => $e->getTraceAsString()
            ];
        }
    }

    static public function checkMinMax(int $val, int $min, int $max): int
    {
        if (!is_numeric($val)) {
            $val = 0;
        }
        $v = $val < $max ? $val : $max;
        $v = $v > $min ? $v : $min;
        return $v;
    }
}
