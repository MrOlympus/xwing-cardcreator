<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class ShipStatProvider extends ServiceProvider
{

    /**
     * @return string[]
     */
    public static function shipStats(string $shipName): array
    {
        $data = json_decode(file_get_contents(resource_path() . '/files/shipStats.json'), true);
        return $data[$shipName] ?? [];
    }

    /**
     * @return string[][]
     */
    public static function getShipFullNameAndSymbol(string $shipName): array
    {
        $data = json_decode(file_get_contents(resource_path() . '/files/shipName.json'), true);
//        dd([
//            "data" => $data,
//            "name" => $shipName,
//            "return" => $data[$shipName] ?? []
//        ]);
        return $data[$shipName] ?? [];
    }
}
