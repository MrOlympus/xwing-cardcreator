<?php

namespace App\Providers;

define('CARD_UPGRADE_W', 418);
define('CARD_UPGRADE_H', 300);
define('CARD_ART_UPGRADE_W', 238);
define('CARD_ART_UPGRADE_H', 120);

use Exception;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class UpgradeCardCreatorProvider extends ServiceProvider implements CardCreator
{

    public function register()
    {
    }

    public static function processFiles(array $files): array
    {
        $processedFiles = [];

        if (isset($files['card-art-image'])) {
            if ($files['card-art-image']['type'] == 'image/jpeg') {
                $processedFiles['imageCardArt'] = imagecreatefromjpeg($files['card-art-image']['tmp_name']);
            } elseif ($files['card-art-image']['type'] == 'image/png') {
                $processedFiles['imageCardArt'] = imagecreatefrompng($files['card-art-image']['tmp_name']);
            }
        } else {
            $processedFiles['imageCardArt'] = imagecreatetruecolor(1, 1);
        }

        return $processedFiles;
    }

    public static function processParams(array $params): array
    {
        $params['card-name'] = isset($params['card-name']) && $params['card-name'] != '' ? $params['card-name'] : '';
        $params['card-type'] = isset($params['card-type']) && $params['card-type'] != '' ? $params['card-type'] : 'ept';
        $params['arc-type'] = isset($params['arc-type']) && $params['arc-type'] != '' ? $params['arc-type'] : 'classic';
        $params['grant-attack'] = isset($params['grant-attack']) && $params['grant-attack'] != '' && $params['grant-attack'] != 'false' ? true : false;
        $params['grant-action'] = isset($params['grant-action']) && $params['grant-action'] != '' && $params['grant-action'] != '' ? $params['grant-action'] : 'none';
        $params['grant-action-red'] = isset($params['grant-action-red']) && $params['grant-action-red'] != '' && $params['grant-action-red'] != 'false' ? true : false;

        $params['dice-number'] = isset($params['dice-number']) && $params['dice-number'] != '' ? intval($params['dice-number']) : 0;
        $params['dice-number'] = self::checkMinMax($params['dice-number'], 0, 9);

        $params['range-bonus'] = isset($params['range-bonus']) && $params['range-bonus'] != '' && $params['range-bonus'] != 'false' ? true : false;

        $params['min-range'] = isset($params['min-range']) && $params['min-range'] != '' ? intval($params['min-range']) : 0;
        $params['min-range'] = self::checkMinMax($params['min-range'], 0, 5);
        $params['max-range'] = isset($params['max-range']) && $params['max-range'] != '' ? intval($params['max-range']) : 0;
        $params['max-range'] = self::checkMinMax($params['max-range'], 0, 5);

        $params['charge-number'] = isset($params['charge-number']) && $params['charge-number'] != '' ? intval($params['charge-number']) : 0;
        $params['charge-number'] = self::checkMinMax($params['charge-number'], 0, 9);
        $params['charge-regenerate'] = isset($params['charge-regenerate']) && $params['charge-regenerate'] != '' && $params['charge-regenerate'] != 'false' ? true : false;

        $params['force-number'] = isset($params['force-number']) && $params['force-number'] != '' ? intval($params['force-number']) : 0;
        $params['force-number'] = self::checkMinMax($params['force-number'], 0, 9);
        $params['force-regenerate'] = isset($params['force-regenerate']) && $params['force-regenerate'] != '' && $params['force-regenerate'] != 'false' ? true : false;

        $params['hull-number'] = isset($params['hull-number']) && $params['hull-number'] != '' ? intval($params['hull-number']) : 0;
        $params['hull-number'] = self::checkMinMax($params['hull-number'], 0, 6);

        $params['shield-number'] = isset($params['shield-number']) && $params['shield-number'] != '' ? intval($params['shield-number']) : 0;
        $params['shield-number'] = self::checkMinMax($params['shield-number'], 0, 6);

        $params['card-text'] = (isset($params['card-text']) && $params['card-text'] != '' ? $params['card-text'] : '');
        $params['restrictions-text'] = (isset($params['restrictions-text']) && $params['restrictions-text'] != '' ? $params['restrictions-text'] : '');

        return $params;
    }

    public static function createCard(array $params, array $files, string $id, bool $privatePath = false)
    {
        try {
            // ----------------------------------- Font loading
            $fonts = [
                'main' => CardCreatorProvider::loadRessource('eurostile.ttf'),
                'title' => CardCreatorProvider::loadRessource('bank-gothic-medium-bt.ttf'),
                'bold' => CardCreatorProvider::loadRessource('arialbd.ttf'),
                'italic' => CardCreatorProvider::loadRessource('ariali.ttf'),
                'boldItalic' => CardCreatorProvider::loadRessource('arialbi.ttf'),
                'symbol' => CardCreatorProvider::loadRessource('xwing-miniatures-modified.ttf')
            ];

            // ----------------------------------- Image loading

            $images = [
                'cardType' => 'upgrade-' . $params['card-type'] . '.png',
                'secondaryWeaponBackground' => 'secondary-weapon-background.png',
                'secondaryWeaponArc' => 'secondary-arc-' . $params['arc-type'] . '.png',
                'secondaryRange' => 'secondary-range-bonus.png',
                'charges' => 'bonus-charges.png',
                'grantAction' => 'grantAction.png',
                'chargesRegen' => 'charge-regen.png',
                'force' => 'bonus-forces.png',
                'forceRegen' => 'force-regen.png',
                'hull' => 'bonus-hull.png',
                'shield' => 'bonus-shield.png',
                'restrictionsText' => 'restrictions_text.png'
            ];

            foreach ($images as $name => $path) {
                $images[$name] = CardCreatorProvider::loadImagePng($path);
            }

            $images['cardArt'] = $files['imageCardArt'];

            // ----------------------------------- Background creation

            if ($params['grant-attack'] || $params['charge-number'] > 0 || $params['force-number'] > 0 || $params['grant-action'] != 'none' || $params['hull-number'] > 0 || $params['shield-number'] > 0) {
                $textSize = 'small';
            } else {
                $textSize = 'large';
            }

            $imBase = CardCreatorProvider::loadImagePng('base.png');
            $imTextBackground = CardCreatorProvider::loadImagePng($textSize . '_text.png');
            $im = imagecreatetruecolor(CARD_UPGRADE_W, CARD_UPGRADE_H);
            imagefill($im, 0, 0, 0x7fff0000);
            imagecopyresampled($im, $imBase, 0, 0, 0, 0, CARD_UPGRADE_W, CARD_UPGRADE_H, CARD_UPGRADE_W, CARD_UPGRADE_H);
            imagecopyresampled($im, $images['cardType'], 30, 28, 0, 0, 102, 103, 102, 103);

            // ----------------------------------- Color creation

            $background_color = imagecolorallocate($im, 0, 0, 0);
            $color_white = imagecolorallocate($im, 255, 255, 255);
            $color_darkGrey = imagecolorallocate($im, 70, 70, 70);
            $color_black = imagecolorallocate($im, 46, 46, 46);
            $color_red = imagecolorallocate($im, 238, 32, 36);

            imagesavealpha($im, true);

            // ----------------------------------- Card name

            $processedText = CardCreatorProvider::processTextSymbols($params['card-name'], 10, $fonts, 'title');
            CardCreatorProvider::writeTextBlockCenter($im, 166, 403, 123, 144, $processedText, $color_white);

            // ----------------------------------- Card art

            $src_w = imagesx($images['cardArt']);
            $src_h = imagesy($images['cardArt']);

            if (imagesx($images['cardArt']) / ($images['cardArt']) < CARD_ART_UPGRADE_W / CARD_ART_UPGRADE_H) {
                $src_h = $src_h / ((CARD_ART_UPGRADE_W / CARD_ART_UPGRADE_H) / (imagesx($images['cardArt']) / imagesy($images['cardArt'])));
            } else {
                $src_w = $src_w * ((CARD_ART_UPGRADE_W / CARD_ART_UPGRADE_H) / (imagesx($images['cardArt']) / imagesy($images['cardArt'])));
            }

            imagecopyresampled($im, $images['cardArt'], 167, 1, 0, 0, CARD_ART_UPGRADE_W, CARD_ART_UPGRADE_H, $src_w, $src_h);

            // ----------------------------------- Secondary weapons (right part)

            if ($params['grant-attack']) {
                imagecopyresampled($im, $images['secondaryWeaponBackground'], 342, 147, 0, 0, 58, 52, 58, 52);
                imagecopyresampled($im, $images['secondaryWeaponArc'], 349, 153, 0, 0, 19, 19, 19, 19);

                imagettftext($im, 17, 0, 374, 171, $color_red, $fonts['title'], $params['dice-number']);

                if (!$params['range-bonus']) {
                    imagecopyresampled($im, $images['secondaryRange'], 345, 182, 0, 0, 18, 8, 18, 8);
                }

                $text_color = imagecolorallocate($im, 255, 255, 255);
                if ($params['min-range'] != $params['max-range']) {
                    imagettftext($im, 10, 0, 365, 191, $text_color, $fonts['title'], $params['min-range'] . '-' . $params['max-range']);
                } else {
                    imagettftext($im, 10, 0, 375, 191, $text_color, $fonts['title'], $params['min-range']);
                }
            }

            // ----------------------------------- Main text

            imagecopyresampled($im, $imTextBackground, 0, 0, 0, 0, CARD_UPGRADE_W, CARD_UPGRADE_H, CARD_UPGRADE_W, CARD_UPGRADE_H);

            $x_left = 180;
            $x_right = $textSize == 'large' ? 390 : 328;

            $y_line = 151;
            $y_max = 283;

            $cardText_fontSize = 10;

            $content = CardCreatorProvider::processTextSymbols($params['card-text'], $cardText_fontSize, $fonts);
            CardCreatorProvider::writeTextBlockCenter($im, $x_left, $x_right, $y_line, $y_max, $content, $color_black);
            //340, 199

            // ----------------------------------- Grant action

            if ($params['grant-action'] != 'none') {

                imagecopyresampled($im, $images['grantAction'], 338, 199, 0, 0, 67, 36, 67, 36);

                $content = CardCreatorProvider::processTextSymbols($params['grant-action'], 13, $fonts);
                $color = $params['grant-action-red'] ? $color_red : $color_white;

                //writeTextBlockCenter($im, 344, 407, 216, 243, $content, $color);
                CardCreatorProvider::writeTextBlockCenter($im, 348, 404, 202, 231, $content, $color);
            }

            // ----------------------------------- Charges

            if ($params['charge-number'] != 0) {
                if ($params['grant-attack']) {
                    $y_charge = 220;
                } else {
                    $y_charge = 147;
                }
                imagecopyresampled($im, $images['charges'], 343, $y_charge, 0, 0, 29, 33, 29, 33);
                $text_color = imagecolorallocate($im, 229, 185, 34);
                imagettftext($im, 17, 0, 375, $y_charge + 25, $text_color, $fonts['title'], $params['charge-number']);
                if ($params['charge-regen']) {
                    imagecopyresampled($im, $images['chargesRegen'], 395, $y_charge + 5, 0, 0, 7, 8, 7, 8);
                }
            }

            // ----------------------------------- Force

            if ($params['force-number'] != 0) {
                if ($params['grant-attack']) {
                    $y_force = 220;
                } else {
                    if ($params['charge-number'] != 0) {
                        $y_force = 190;
                    } else {
                        $y_force = 147;
                    }
                }
                imagecopyresampled($im, $images['force'], 343, $y_force, 0, 0, 29, 33, 29, 33);
                $text_color = imagecolorallocate($im, 186, 150, 185);
                imagettftext($im, 17, 0, 375, $y_force + 25, $text_color, $fonts['title'], $params['force-number']);
                if ($params['force-regen']) {
                    imagecopyresampled($im, $images['forceRegen'], 395, $y_force + 5, 0, 0, 7, 8, 7, 8);
                }
            }

            // ----------------------------------- Hull

            if ($params['hull-number'] != 0) {
                $y_hull = 147;
                imagecopyresampled($im, $images['hull'], 343, $y_hull, 0, 0, 29, 33, 29, 33);
                $text_color = imagecolorallocate($im, 240, 233, 29);
                imagettftext($im, 17, 0, 375, $y_hull + 25, $text_color, $fonts['title'], $params['hull-number']);
            }

            // ----------------------------------- Shield

            if ($params['shield-number'] != 0) {
                if ($params['hull-number'] != 0) {
                    $y_shield = 190;
                } else {
                    $y_shield = 147;
                }
                imagecopyresampled($im, $images['shield'], 343, $y_shield, 0, 0, 29, 33, 29, 33);
                $text_color = imagecolorallocate($im, 125, 209, 226);
                imagettftext($im, 17, 0, 375, $y_shield + 25, $text_color, $fonts['title'], $params['shield-number']);
            }


            // ----------------------------------- Restrictions text

            if ($params['restrictions-text'] != '') {
                imagecopyresampled($im, $images['restrictionsText'], 17, 246, 0, 0, 129, 53, 129, 53);

                $x_left = 22;
                $x_right = 139;

                $y_line = 251;
                $y_max = 298;

                $processedText = CardCreatorProvider::processTextSymbols($params['restrictions-text'], 9, $fonts, 'italic');
                CardCreatorProvider::writeTextBlockCenter($im, $x_left, $x_right, $y_line, $y_max, $processedText, $color_black);
            }

            // ----------------------------------- Rounding corners


            $processedText = CardCreatorProvider::processTextSymbols($params['card-name'], 10, $fonts, 'title');
            CardCreatorProvider::writeTextBlockCenter($im, 166, 403, 123, 144, $processedText, $color_white);

            // ----------------------------------- Output

            ob_start();
            imagepng($im);
            imagealphablending($im, false);
            imagesavealpha($im, true);
            $contents = ob_get_contents();
            ob_end_clean();

            //  $cardName = str_replace('!LIM', '', $cardName);
            $path = CardCreatorProvider::saveImageOnDiskTemp($contents, $id, $privatePath);
            CardCreatorProvider::cleanTempImages();

            return [
                'success' => true,
                'data' => ['path' => $path]
            ];
        } catch (Exception $e) {
            $message = json_encode([
                'exception' => [
                    'position' => $e->getFile() . ':' . $e->getLine(),
                    'message' => $e->getMessage()
                ],
                'params' => $params,
                'files' => $files,
            ]);
            DiscordMessageProvider::sendMessage(":warning: Error ! Here are some details :", env('DISCORD_HOOK_LOGS'));
            DiscordMessageProvider::sendMessage("```JSON\n" . $message . "\n```", env('DISCORD_HOOK_LOGS'));
            return [
                'success' => false,
                'exception' => $e->getFile() . ' ' . $e->getLine() . ' : ' . $e->getMessage(),
                'full' => $e->getTraceAsString()
            ];
        }
    }

    static public function checkMinMax(int $val, int $min, int $max): int
    {
        if (!is_numeric($val)) {
            $val = 0;
        }
        $v = $val < $max ? $val : $max;
        $v = $v > $min ? $v : $min;
        return $v;
    }
}
