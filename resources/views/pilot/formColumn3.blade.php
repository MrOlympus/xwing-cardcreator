<div class="col s12 l4">
    <label>Actions</label>
    <div class="col s12 actionSelectRow">
        <div class="input-field col s5" id="actionSelect01">
            <select>
                <option value="!foc" selected>f</option>
                <option value="!cal">a</option>
            </select>
        </div>
        <div class="col s2">
            <span><i class="material-icons">chevron_right</i></span>
        </div>
        <div class="input-field col s5" id="red-actionSelect01">
            <select>
                <option value="none" selected></option>
                <option value="!foc">f</option>
                <option value="!cal">a</option>
            </select>
        </div>
    </div>
    <div class="col s12 actionSelectRow">
        <div class="input-field col s5" id="actionSelect02">
            <select>
                <option value="none" selected></option>
                <option value="!foc">f</option>
                <option value="!cal">a</option>
                <option value="!tlk">l</option>
                <option value="!boo">b</option>
                <option value="!bar">r</option>
                <option value="!eva">e</option>
                <option value="!rei">i</option>
                <option value="!rel">=</option>
                <option value="!rot">R</option>
                <option value="!jam">j</option>
                <option value="!clk">k</option>
                <option value="!coo">o</option>
                <option value="!sla">s</option>
            </select>
        </div>
        <div class="col s2">
            <span><i class="material-icons">chevron_right</i></span>
        </div>
        <div class="input-field col s5" id="red-actionSelect02">
            <select>
                <option value="none" selected></option>
                <option value="!foc">f</option>
                <option value="!cal">a</option>
                <option value="!tlk">l</option>
                <option value="!boo">b</option>
                <option value="!bar">r</option>
                <option value="!eva">e</option>
                <option value="!rei">i</option>
                <option value="!rel">=</option>
                <option value="!rot">R</option>
                <option value="!jam">j</option>
                <option value="!clk">k</option>
                <option value="!coo">o</option>
                <option value="!sla">s</option>
            </select>
        </div>
    </div>
    <div class="col s12 actionSelectRow">
        <div class="input-field col s5" id="actionSelect03">
            <select>
                <option value="none" selected></option>
                <option value="!foc">f</option>
                <option value="!cal">a</option>
                <option value="!tlk">l</option>
                <option value="!boo">b</option>
                <option value="!bar">r</option>
                <option value="!eva">e</option>
                <option value="!rei">i</option>
                <option value="!rel">=</option>
                <option value="!rot">R</option>
                <option value="!jam">j</option>
                <option value="!clk">k</option>
                <option value="!coo">o</option>
                <option value="!sla">s</option>
            </select>
        </div>
        <div class="col s2">
            <span><i class="material-icons">chevron_right</i></span>
        </div>
        <div class="input-field col s5" id="red-actionSelect03">
            <select>
                <option value="none" selected></option>
                <option value="!foc">f</option>
                <option value="!cal">a</option>
                <option value="!tlk">l</option>
                <option value="!boo">b</option>
                <option value="!bar">r</option>
                <option value="!eva">e</option>
                <option value="!rei">i</option>
                <option value="!rel">=</option>
                <option value="!rot">R</option>
                <option value="!jam">j</option>
                <option value="!clk">k</option>
                <option value="!coo">o</option>
                <option value="!sla">s</option>
            </select>
        </div>
    </div>
    <div class="col s12 actionSelectRow">
        <div class="input-field col s5" id="actionSelect04">
            <select>
                <option value="none" selected></option>
                <option value="!foc">f</option>
                <option value="!cal">a</option>
                <option value="!tlk">l</option>
                <option value="!boo">b</option>
                <option value="!bar">r</option>
                <option value="!eva">e</option>
                <option value="!rei">i</option>
                <option value="!rel">=</option>
                <option value="!rot">R</option>
                <option value="!jam">j</option>
                <option value="!clk">k</option>
                <option value="!coo">o</option>
                <option value="!sla">s</option>
            </select>
        </div>
        <div class="col s2">
            <span><i class="material-icons">chevron_right</i></span>
        </div>
        <div class="input-field col s5" id="red-actionSelect04">
            <select>
                <option value="none" selected></option>
                <option value="!foc">f</option>
                <option value="!cal">a</option>
                <option value="!tlk">l</option>
                <option value="!boo">b</option>
                <option value="!bar">r</option>
                <option value="!eva">e</option>
                <option value="!rei">i</option>
                <option value="!rel">=</option>
                <option value="!rot">R</option>
                <option value="!jam">j</option>
                <option value="!clk">k</option>
                <option value="!coo">o</option>
                <option value="!sla">s</option>
            </select>
        </div>
    </div>
    <div class="col s12 actionSelectRow">
        <div class="input-field col s5" id="actionSelect05">
            <select>
                <option value="none" selected></option>
                <option value="!foc">f</option>
                <option value="!cal">a</option>
                <option value="!tlk">l</option>
                <option value="!boo">b</option>
                <option value="!bar">r</option>
                <option value="!eva">e</option>
                <option value="!rei">i</option>
                <option value="!rel">=</option>
                <option value="!rot">R</option>
                <option value="!jam">j</option>
                <option value="!clk">k</option>
                <option value="!coo">o</option>
                <option value="!sla">s</option>
            </select>
        </div>
        <div class="col s2">
            <span><i class="material-icons">chevron_right</i></span>
        </div>
        <div class="input-field col s5" id="red-actionSelect05">
            <select>
                <option value="none" selected></option>
                <option value="!foc">f</option>
                <option value="!cal">a</option>
                <option value="!tlk">l</option>
                <option value="!boo">b</option>
                <option value="!bar">r</option>
                <option value="!eva">e</option>
                <option value="!rei">i</option>
                <option value="!rel">=</option>
                <option value="!rot">R</option>
                <option value="!jam">j</option>
                <option value="!clk">k</option>
                <option value="!coo">o</option>
                <option value="!sla">s</option>
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col s6">
            <label for="attack">Primary attack <i class="xwing-miniatures-font xwing-miniatures-font-frontarc"></i><span id="attack-front-visualizer" class="badge" title="Primary front attack"></span></label>
            <input type="range" name="attack-front" id="attack-front" min="0" max="5" value="3"/>
        </div>
        <div class="col s6">
            <label for="attack">Primary attack <i class="xwing-miniatures-font xwing-miniatures-font-reararc"></i><span id="attack-rear-visualizer" class="badge" title="Primary rear attack"></span></label>
            <input type="range" name="attack-rear" id="attack-rear" min="0" max="5" value="0"/>
        </div>
        <div class="col s6">
            <label for="attack">Primary attack <i class="xwing-miniatures-font xwing-miniatures-font-singleturretarc"></i><span id="attack-singleMobile-visualizer" class="badge" title="Primary single turret attack"></span></label>
            <input type="range" name="attack-singleMobile" id="attack-singleMobile" min="0" max="5" value="0"/>
        </div>
        <div class="col s6">
            <label for="attack">Primary attack <i class="xwing-miniatures-font xwing-miniatures-font-doubleturretarc"></i><span id="attack-doubleMobile-visualizer" class="badge" title="Primary double turret attack"></span></label>
            <input type="range" name="attack-doubleMobile" id="attack-doubleMobile" min="0" max="5" value="0"/>
        </div>
        <div class="col s6">
            <label for="agility">Agility<span id="agility-visualizer" class="badge" title="Agility"></span></label>
            <input type="range" name="agility" id="agility" min="0" max="5" value="2"/>
        </div>
        <div class="col s6">
            <label for="hull">Hull<span id="hull-visualizer" class="badge" title="Hull"></span></label>
            <input type="range" name="hull" id="hull" min="1" max="15" value="4"/>
        </div>
        <div class="col s6">
            <label for="shield">Shield<span id="shield-visualizer" class="badge" title="Shield"></span></label>
            <input type="range" name="shield" id="shield" min="0" max="10" value="0"/>
        </div>
        <div class="col s6">
            <label for="force">Force<span id="force-visualizer" class="badge" title="Force"></span></label>
            <input type="range" id="force" name="force" min="0" max="6" value="0"/>
        </div>
        <div class="row col s12">
            <div class="col s8">
                <label for="charge">Charges<span id="charge-visualizer" class="badge" title="Charges"></span></label>
                <input type="range" id="charge" name="charge" min="0" max="6" value="0"/>
            </div>
            <div class="col s4">
                <label for="force-regenerate">Regenerate</label>
                <div class="switch">
                    <label>No<input type="checkbox" name="charges-regenerate"><span class="lever"></span>Yes</label>
                </div>
            </div>
        </div>
    </div>

</div>
