<div class="js-cookie-consent cookie-consent row">
    <div class="col s10">
        <span class="cookie-consent__message">
        {{ __('Your experience on this site will be improved by allowing cookies. By continuing, you agree to our use of cookies – find out more') }}
        <a href="/{{ App::getlocale() }}/about/cookies">{{ __('here') }}</a>.
    </span>
    </div>
    <div class="col s2">
        <button class="js-cookie-consent-agree cookie-consent__agree btn">
            {{ ucfirst(__('accept')) }}
        </button>
    </div>
</div>
